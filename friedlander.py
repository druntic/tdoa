from matplotlib import pyplot as plt
import math
from math import *
import numpy as np
import random
import timeit
import audio
from scipy.io import wavfile
import scipy.signal as sps


import plot
def distance(X1,X2):
    return math.sqrt((X1[0]-X2[0])**2+(X1[1]-X2[1])**2+(X1[2]-X2[2])**2)

def getTime(m, source):#in seconds
    return distance(m,source)/340.29
def S(j,sensors):
    S=[]
    for i in range(len(sensors)):
        
        if i!=j:
            S.append([sensors[i][0]-sensors[j][0],sensors[i][1]-sensors[j][1],sensors[i][2]-sensors[j][2]])
    return np.array(S)

def r_ij(tdoa,i,j):
    # tdoa=[]
    # for x in range(len(times)):
    #     tdoa.append(abs(times[x]-times[j]))
    v=340.29
    return v*tdoa[i]
def mi(j,tdoa,sensors,origin):
    mi=[]
    for i in range(len(sensors)):
        if i!=j:
            mi.append([0.5*(distance(sensors[i],origin))**2-0.5*(distance(sensors[j],origin))**2-0.5*(r_ij(tdoa, i, j))**2])
    return np.array(mi)
def rho(tdoa,j):
    rho=[]
    for i in range(len(tdoa)):
        if i!=j:
            rho.append(r_ij(tdoa, i, j));

    return np.array(rho)
def D(j,tdoa):
    return np.linalg.inv(np.diag(rho(tdoa,j)))
def Z(n):
    Z=np.diag(np.ones(n-1), 1)
    Z[n-1][0]=1
    return Z
def I(n):
    return np.diag(np.ones(n), 0)
def M(j,tdoa):
    D1=D(j,tdoa)
    Z1=Z(len(tdoa)-1)
    B=np.subtract(I(len(tdoa)-1), Z1)
    return np.matmul(B, D1)

def getLocation(origin,sensors,tdoa,j):

    if(len(tdoa)-np.count_nonzero(tdoa)>1):
        i=tdoa.index(0)
        tdoa[i]=0.000001
        #print("corrected tdoa:",tdoa)
    #print("last j:",j)
    for i in range(len(sensors)):
        #print(tdoa[i])
        if tdoa[i]==0:
            j=i

    
    # print(np.diag(rho(tdoa,j)))
    # print(tdoa)
    # j=2
    # tdoa=[0.05,0.03,0.02,0.01,0.015,0.022,0.032,0.041,0.25]
    M1=M(j,tdoa)
    mi1=mi(j, tdoa,sensors,origin)
    S1=S(j,sensors)
    #x1=getNewX(S1, M1, mi1)
    #print("another x:",x1)
    A=np.matmul(M1, S1)
    b=M1.dot(mi1)
    x, residuals, rank, s = np.linalg.lstsq(A,b, rcond=None)
    newx=[]
    for i in x:
        newx.append(i[0])
    return newx
def getFirstMicrophone(source,sensors):
    j=0
    for i in range(len(sensors)):
        if distance(sensors[j],source)>distance(sensors[i],source):
            j=i
    return j

def getNewX(S,M,mi):
    A=np.matmul(np.transpose(S),np.transpose(M))
    B=np.matmul(M, S)
    C=np.linalg.inv(np.matmul(A, B))
    #print(np.matmul(C, np.matmul(A, np.matmul(M, mi))))
    return np.matmul(C, np.matmul(A, np.matmul(M, mi)))


