# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 08:03:43 2022

@author: David
"""
from scipy.io import wavfile
import matplotlib.pyplot as plt
from scipy import signal
import numpy as np
import xcorr
import random
import scipy.signal as sps
import friedlander
import math

def noisePower(data, SNR):
    totalPow=0
    for x in data:
        totalPow+=np.real(x)**2+np.imag(x)**2#this is the same as abs(x)**2
    signalPower_lin=totalPow/(len(data))
    #print("average power: ", signalPower_lin)
    signalPower_dB=10*math.log10(signalPower_lin)   
    noisePower_db=signalPower_dB - SNR
    
    return 10**(noisePower_db/10)

def shift(data, shift):
    #print(shift)
    shiftedData=data.copy()
    for i in range(abs(shift)):
        shiftedData=np.insert(shiftedData,0,0)
    return shiftedData

#samplerate*duration of video in sec=len(data)

def getTDOA(x,y, samplerate):#difference in time of arrival between samples x and y
    [lags,corr]=xcorr.xcorr(x, y)
    i=np.argmax(corr)
    #print(lags[i])#lags in samples
    return lags[i]/samplerate

def createsamples(data,tdoa,sampling_rate, SNR):
    samples=[]
    for x in tdoa:
        
        shiftedData=shift(data,int(x*sampling_rate ))
        #print("true lag: ",x*sampling_rate)
        noisePower_lin=noisePower(data, SNR)
        noise = np.random.normal(0,math.sqrt(noisePower_lin),len(shiftedData))
        samples.append(shiftedData+noise)
    return samples
def generatedTDOAs(data,tdoa,sampling_rate,j, SNR):#j first sensor that heard sound
     generatedtdoa=[]
     fakesamples=createsamples(data, tdoa, sampling_rate, SNR)
     for x in range(len(tdoa)):
        #print(len(fakesamples[x]),len(fakesamples[j]))
        
        lags,c=xcorr.xcorr(fakesamples[x],fakesamples[j])  
        
        i=np.argmax(c)
        #print("generated lag:",lags[i])
        #plt.figure()
        #plt.plot(abs(c))
        generatedtdoa.append((lags[i]/sampling_rate))
     return generatedtdoa
def realTDOAs(sensors, source,j):#j inital sensor
    times=[]
    for x in sensors:
        times.append(friedlander.getTime(x, source))
    tdoa=[]
    for x in times:
        tdoa.append((x-times[j]))
    return tdoa
if __name__ == "__main__":
    # Read file
    sampling_rate, data = wavfile.read('sample.wav')
    # Your new sampling rate
    new_rate = 44100
    # Resample data
    number_of_samples = round(len(data) * float(new_rate) / sampling_rate)
    data = sps.resample(data, number_of_samples)
    data=data[:,0]
    data= np.array(data, dtype=np.float64)
    sampling_rate=new_rate
    #plt.plot(data)
    
    # label the axes
    plt.ylabel("Amplitude")
    plt.xlabel("Samples")
    # set the title  
    plt.title("Sample Wav")
    # display the plot
    
    SNR=100
    sensors=[[2,2,0],[4,2,0],[2,4,0],[4,4,0]]
    source=[-10,0,0]
    j=0
    tdoa=realTDOAs(sensors, source, j)
    
    fakesamples=createsamples(data, [0.0, 0.005808606080503431, 0.1, 0.1], sampling_rate, SNR)
    for x in [fakesamples[0]]:
        print(np.argmax(x))
        #plt.plot(x)
    #plt.plot(np.random.normal(0,math.sqrt(noisePower(data, SNR)),len(fakesamples[0])), alpha=0.5)
    
    plt.show()
    # lags,c=xcorr.xcorr(fakesamples[0],fakesamples[2])    
    # i=np.argmax(c)
    # print(lags[i])
    genTDOAs=generatedTDOAs(data,tdoa,sampling_rate,j, SNR)
    print("real TDOAs: ",tdoa)
    print("generated TDOAs: ",genTDOAs)
    #print([x/y if y!=0 else 0 for x,y in zip(tdoa,genTDOAs)])
    
    scaled = np.int16(fakesamples[0]/np.max(np.abs(fakesamples[0])) * 32767)
    wavfile.write('test.wav', 44100, scaled)