from matplotlib import pyplot as plt
import math
from math import *
import numpy as np


def plot2d(sensors, source, newx):
    plt.xlim(-9, 9)
    plt.ylim(-9, 9)
    labels=[]
    for i in range(len(sensors)):
        labels.append("M"+str(i+1))
    #plt.rcParams["figure.figsize"] = [7.00, 7]
    #plt.rcParams["figure.autolayout"] = True
    #plt.xlim(-20, 20)
    #plt.ylim(-20, 20)
    plt.grid()
    for i in range(len(labels)):
        plt.plot(sensors[i][0], sensors[i][1], marker="o", markersize=5,color="blue")
        plt.annotate(labels[i], # this is the text
                      (sensors[i][0],sensors[i][1]),ha='center',xytext=(0,10),textcoords="offset points") # these are the coordinates to position the label)

    plt.plot(source[0], source[1], marker="o", markersize=5,color="red")
    plt.annotate("S", # this is the text
                  (source[0],source[1]),ha='center',xytext=(0,10),textcoords="offset points")
    plt.plot(newx[0], newx[1], marker="o", markersize=5,color="purple")
    plt.annotate("possible S", # this is the text
                  (newx[0],newx[1]),ha='center',xytext=(0,10),textcoords="offset points")

    
def axes():
    plt.axhline(0, alpha=.1)
    plt.axvline(0, alpha=.1)
def col_cycler(cols):
    count = 0
    while True:
        yield cols[count]
        count = (count + 1)%len(cols)
def angle_trunc(a):
    while a < 0.0:
        a += np.pi * 2
    return a

def getAngleBetweenPoints(x_orig, y_orig, x_landmark, y_landmark):
    deltaY = y_landmark - y_orig
    deltaX = x_landmark - x_orig
    return angle_trunc(atan2(deltaY, deltaX))
def plotHyperbolas(sensors,tdoa,j):
    plt.rcParams["figure.figsize"] = [7.00, 7]
    plt.rcParams["figure.autolayout"] = True
    
    x = np.linspace(-10, 10, 400)
    y = np.linspace(-10, 10, 400)
    x, y = np.meshgrid(x, y)
    col_iter = col_cycler(['c','m', 'y','k'])
    axes()
    for i in range(len(sensors)):
        if i!=j:      
            theta=-getAngleBetweenPoints(sensors[i][0], sensors[i][1], sensors[j][0], sensors[j][1])
            a=340.29*tdoa[i]/2
            c=0.5*math.sqrt((sensors[j][0]-sensors[i][0])**2+(sensors[j][1]-sensors[i][1])**2)
            #print("c: ", c)
            b=math.sqrt(c**2-a**2)
            x0=(sensors[j][0]+sensors[i][0])/2
            y0=(sensors[j][1]+sensors[i][1])/2
            #plt.contour(x, y,((x-(sensors[j][0]+sensors[i][0])/2)**2/a**2 - (y-(sensors[i][1]+sensors[j][1])/2)**2/b**2), [1],colors=next(col_iter))
            plt.contour(x, y,(((np.cos(theta)*(x-x0)-np.sin(theta)*(y-y0)))**2/a**2 - ((np.sin(theta)*(x-x0)+np.cos(theta)*(y-y0)))**2/b**2), [1],colors=next(col_iter))


def plot3d(sensors, source, newx):
    labels=[]
    for i in range(len(sensors)):
        labels.append("M"+str(i+1))
    fig = plt.figure()
    
    ax = fig.add_subplot(122, projection='3d')
    ax.set_xlim3d(-15, 15)
    ax.set_ylim3d(-15, 15)
    ax.set_zlim3d(-15, 15)
    for i in range(len(sensors)): #plot each point + it's index as text above
        ax.scatter(sensors[i][0],sensors[i][1],sensors[i][2],color='blue') 
        ax.text(sensors[i][0],sensors[i][1],sensors[i][2],  '%s' % labels[i], size=10, zorder=1,  
        color='k') 
    ax.scatter(source[0],source[1],source[2],color='purple') 
    ax.text(source[0],source[1],source[2],  '%s' % "S", size=10, zorder=1,  
    color='k')
    ax.scatter(newx[0],newx[1],newx[2],color='red') 
    ax.text(newx[0],newx[1],newx[2],  '%s' % "possible S", size=10, zorder=1,  
    color='k')
    
    plt.show()
    
def plot3dAllSensors(allSensors):
    fig = plt.figure()
    for i in range(len(allSensors)):
        number=231+i
        sensors=allSensors[i]
        labels=[]
        for i in range(len(sensors)):
            labels.append("M"+str(i+1))
        
        
        ax = fig.add_subplot(number, projection='3d')
        ax.set_xlim3d(-15, 15)
        ax.set_ylim3d(-15, 15)
        ax.set_zlim3d(-15, 15)
        for i in range(len(sensors)): #plot each point + it's index as text above
            ax.scatter(sensors[i][0],sensors[i][1],sensors[i][2],color='blue') 
            ax.text(sensors[i][0],sensors[i][1],sensors[i][2],  '%s' % labels[i], size=10, zorder=1,  
            color='k')
