import friedlander
import audio
import newton
from scipy.io import wavfile
import scipy.signal as sps
import timeit
import numpy as np
import plot
from matplotlib import pyplot as plt
import random
import newton
import statistics

if __name__ == "__main__":
    sensors=[[0,0,0],[1,0,0],[0,1,0],[1,2,0]]
    source=[-8, -6, 0]
    origin=[0,0,0]
    j=friedlander.getFirstMicrophone(source, sensors)#first microphone that heard sound
    
    # Read file
    sampling_rate, data = wavfile.read('192 chirp.wav')
    # Your new sampling rate
    new_rate = 44100



    # Resample data
    number_of_samples = round(len(data) * float(new_rate) / sampling_rate)
    data = sps.resample(data, number_of_samples)
    data= np.array(data, dtype=np.float64)    
    SNR=10

        
    realtdoa=audio.realTDOAs(sensors, source, j)
    #simulated time difference of arrival
    tdoa=audio.generatedTDOAs(data, realtdoa, sampling_rate, j, SNR)
    print("true tdoa:", realtdoa)
    print("generated tdoa:", tdoa)
    start = timeit.default_timer()
    newx=friedlander.getLocation(origin, sensors, tdoa, j)
    #try to improve solution using newtons method
    newx=newton.improvesolution(newx, sensors, tdoa, j)

    
    stop = timeit.default_timer()

    print("real source location:",source)
    print("possible source location:",newx)
    print("distance between S and possible S: ",friedlander.distance(source, newx))
    #print('Time: ', (stop - start)*1000,"ms")
    # #plotting the sensors, real location and possible location
    #2d
    plot.plotHyperbolas(sensors, tdoa,j)
    plot.plot2d(sensors, source, newx)
    #3d
    #plot.plot3d(sensors4, source, newx)

    