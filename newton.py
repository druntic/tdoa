import numpy as np
from friedlander import distance

def fi(x,sensors,tdoa,i,j):
    # print(distance(x,sensors[i]))
    # print(distance(x,sensors[j]))
    # print(distance(x, sensors[i])-distance(x,sensors[j]))
    # print("---")
    return distance(x,sensors[i])-distance(x,sensors[j])-340.29*tdoa[i]

def fder(x,k,sensors,tdoa,i,j):#deriviraj fun f po k toj varijabli
    return (x[k]-sensors[i][k])/distance(x,sensors[i])-(x[k]-sensors[j][k])/distance(x,sensors[j])
#npr der fun f2 po prvoj varijabli x
#fder(x,0,sensors,tdoa,1,j)
def fdermat(x,sensors,tdoa,j):
    mat=[]

    for i in range(len(sensors)):
        row=[]
        if i!=j:
            for k in range(3):
            
                row.append(fder(x,k,sensors,tdoa,i,j))
            mat.append(row)
            
    return np.linalg.pinv(mat)

def improvesolution(x,sensors,tdoa,j):
    vec=[]
    for i in range(len(sensors)):
        if i!=j:
            vec.append(fi(x,sensors,tdoa,i,j))
    newx=x-np.matmul(fdermat(x,sensors,tdoa,j), vec)

    vec2=[]
    for i in range(len(sensors)):
        if i!=j:
            vec2.append(fi(newx,sensors,tdoa,i,j))
    if abs(np.sum(np.matmul(fdermat(x,sensors,tdoa,j), vec)))<abs(np.sum(np.matmul(fdermat(newx,sensors,tdoa,j), vec2))):

        return x
    return newx.tolist()


if __name__ == "__main__":
    sensors=[[0,-5,5],[10,-5,-5],[-10,5,-5],[-10,5,25],[10,5,-5],[0,5,2.5],[-10,-5,0],[10,0,3.75],[0,-2.5,-5]]
    tdoa=[0.06783854166666667, 0.10455208333333334, 0.08344270833333334, 0.0, 0.10123958333333333, 0.07147395833333334, 0.07285416666666666, 0.07856770833333333, 0.0948125]
    realsource=[-3074, 1145, 9603]
    source=[-3350.379797755356, 1248.7091088437228, 10467.068866398773]
    realtdoa=[0.06783930089335755, 0.1045571327241035, 0.08344744163995799, 0.0, 0.10124390228307689, 0.07147433953005233, 0.072854404722964, 0.0785711825026496, 0.09481551158799562]
    #tdoa=realtdoa
    #print(fder(source,0,sensors,tdoa,1,3))
    #print(fdermat(source,sensors,tdoa,3))
    
    realsource=[644, 2395, 9925]
    source=[548.8182245518005, 2040.923406628529, 8458.529950246419]
    tdoa=[0.06203125, 0.088703125, 0.08552604166666666, 0.0, 0.081828125, 0.062276041666666664, 0.07815104166666667, 0.06031770833333333, 0.08881770833333333]
    
    realsource=[20300, -4410, -21905]
    source=[9719.102377283409, -2109.4704978170193, -10488.59892056892]
    tdoa=[0.04108333333333333, 0.0, 0.04381770833333333, 0.10778125, 0.004291666666666667, 0.04004166666666667, 0.05018229166666666, 0.02080729166666667, 0.020828125]
    
    source=np.array(source)
    j=1
    print("true fi:",fi(realsource, sensors, tdoa, 2, j))
    print("fake fi:",fi(source, sensors, tdoa, 2, j))
    print(distance(realsource, source))
    source=improvesolution(source, sensors, tdoa, j)
    print("true fi:",fi(realsource, sensors, tdoa, 2, j))
    print("fake fi:",fi(source, sensors, tdoa, 2, j))
    print(distance(realsource, source))
    source=improvesolution(source, sensors, tdoa, j)
    print("true fi:",fi(realsource, sensors, tdoa, 2, j))
    print("fake fi:",fi(source, sensors, tdoa, 2, j))
    print(distance(realsource, source))
