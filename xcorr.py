import matplotlib.pyplot as plt
from scipy import signal
import numpy as np

# def xcorr(x, y, normed=True, detrend=False, maxlags=10):
#     if len(x)<len(y):
#         for i in range(len(y)-len(x)):
#             x=np.append(x,0)
#     if len(y)<len(x):
#         for i in range(len(x)-len(y)):
#             y=np.append(y,0)
#     # Cross correlation of two signals of equal length
#     # Returns the coefficients when normed=True
#     # Returns inner products when normed=False
#     # Usage: lags, c = xcorr(x,y,maxlags=len(x)-1)
#     # Optional detrending e.g. mlab.detrend_mean

#     Nx = len(x)
#     if Nx != len(y):
#         raise ValueError('x and y must be equal length')
    
#     if detrend:
#         import matplotlib.mlab as mlab
#         x = mlab.detrend_mean(np.asarray(x)) # can set your preferences here
#         y = mlab.detrend_mean(np.asarray(y))
    
#     c = signal.correlate(x, y, mode='full')

#     if normed:
#         n = np.sqrt(np.dot(x, x) * np.dot(y, y)) # this is the transformation function
#         c = np.true_divide(c,n)

#     if maxlags is None:
#         maxlags = Nx - 1

#     if maxlags >= Nx or maxlags < 1:
#         raise ValueError('maglags must be None or strictly '
#                          'positive < %d' % Nx)

#     lags = np.arange(-maxlags, maxlags + 1)
#     c = c[Nx - 1 - maxlags:Nx + maxlags]
#     return lags, c

def xcorr(x,y):
    """
    Perform Cross-Correlation on x and y
    x    : 1st signal
    y    : 2nd signal

    returns
    lags : lags of correlation
    corr : coefficients of correlation
    """
    corr = signal.correlate(x, y, mode="full")
    lags = signal.correlation_lags(len(x), len(y), mode="full")
    return lags, corr
